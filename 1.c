#include <stdio.h>

int L=1, N;

void line(int y)
{if(y>0)
    {
    printf(" %d",y);
    line(y-1);
    }
}

void pattern(int x){
    if(x>0)
    {
    line(L);
    printf("\n");
    L++;
    pattern(x-1);
    }
}

int main(){

    printf(" NUMBER OF LINES : ");
    scanf("%d",&N);
    pattern(N);
    return 0;
}
